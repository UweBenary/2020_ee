package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.StringService;

@WebServlet("/MC")
public class MainController extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    // Controller
      // hier keine
    // Modell
    StringService stringService = new StringService();
    String generatedString = stringService.generateString();
    // View
    request.setAttribute("generatedString", generatedString);
    request.getRequestDispatcher("stringOutput.jsp").forward(request, response);
  }
}
