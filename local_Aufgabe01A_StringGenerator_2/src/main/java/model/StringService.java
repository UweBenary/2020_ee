package model;

import java.util.Random;

public class StringService {

  public String generateString() {
    Random rand = new Random();
    String allChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    int len = rand.nextInt(100);
    StringBuilder output = new StringBuilder(len);
    for (int i = 0; i < len; i++) {
      int randomInt = rand.nextInt(allChars.length());
      output.append(allChars.charAt(randomInt));
    }
    return output.toString();
  }

}
