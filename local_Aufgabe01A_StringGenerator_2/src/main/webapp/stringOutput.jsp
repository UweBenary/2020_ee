<%-- 
    Document   : stringOutput
    Created on : 05.05.2020, 08:56:44
    Author     : Your Name <yourname@comcave.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Generated String</title>
  </head>
  <body>
    <jsp:include page="header.jsp" />
    <h1>Hello World!</h1>
    <%= request.getAttribute("generatedString") %>
            
  </body>
</html>
