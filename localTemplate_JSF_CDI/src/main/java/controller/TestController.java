package controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import model.TestService;
import util.JsfUtil;

@Named // in JSF Facelet "#{testController}
@RequestScoped
//@Transactional // Only for Transaction Attribute Test
public class TestController {
  
  @Inject
  private TestService testService;
  
  public void test1() {
    String message = testService.getMessage1();
    JsfUtil.addSuccessMessage(message);
//    JsfUtil.addErrorMessage(message);
  }

  public void test2() {
    JsfUtil.addSuccessMessage("Message 2 ...");
  }

  public void test3() {
    JsfUtil.addSuccessMessage("Message 3 ...");
  }


  
  
}
