package listener;

import java.util.stream.IntStream;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import util.ConsoleUtil;

public class JsfListener implements PhaseListener {
    
  @Override
  public void beforePhase(PhaseEvent event) {
    ConsoleUtil.formatTitle("Phase %s - BEFORE", event.getPhaseId());
  }

  @Override
  public void afterPhase(PhaseEvent event) {
    ConsoleUtil.formatTitle("Phase %s - AFTER", event.getPhaseId());
    if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
      IntStream.range(0, 5).forEach(i -> System.out.println("."));
    }
      
  }

  @Override
  public PhaseId getPhaseId() {
    return PhaseId.ANY_PHASE;
  }

}
