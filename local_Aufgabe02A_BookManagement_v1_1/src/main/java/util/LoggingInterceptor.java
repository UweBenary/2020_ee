package util;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Priority;
import javax.interceptor.AroundConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import static util.StringUtil.simpleNameCleaned;

@Interceptor
@Priority(200)
public class LoggingInterceptor {

  
  public LoggingInterceptor() {
    ConsoleUtil.format("%s -> Constructor", simpleNameCleaned(this));
  }

  @AroundConstruct
  private void interceptConstructor(InvocationContext ic) throws Exception {
//    try {
//      loggingOutput("@AroundConstruct", ic);
//    } catch (Exception ex) {
//      loggingOutput("@AroundConstruct");
//    }
    ic.proceed();
  }

  @PostConstruct
  private void interceptPostConstruct(InvocationContext ic) {
//    try {
//      loggingOutput("@PostConstruct", ic);
//    } catch (Exception ex) {
//      loggingOutput("@PostConstruct");
//    }

    try {
      ic.proceed();
    } catch (Exception ex) {
      Logger.getLogger(LoggingInterceptor.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @PreDestroy
  private void interceptPreDestroy(InvocationContext ic) {
//    try {
//      loggingOutput("@PreDestroy", ic);
//    } catch (Exception ex) {
//      loggingOutput("@PreDestroy");
//    }
        
    try {
      ic.proceed();
    } catch (Exception ex) {
      Logger.getLogger(LoggingInterceptor.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @AroundInvoke
  private Object intercept(InvocationContext ic) throws Exception {
    loggingOutput("@AroundInvoke", ic);
    Object obj = ic.proceed();
    return obj;
  }

  private void loggingOutput(String annotation) {
    ConsoleUtil.format("%s -> %s ==> %s", 
      simpleNameCleaned(this), 
      annotation,
      "no InvocationContext"            
    );
  }
  
  private void loggingOutput(String annotation, InvocationContext ic) {
    ConsoleUtil.format("%s -> %s ==> %s", 
      simpleNameCleaned(this), 
      annotation,
      displayInvokationContext(ic)
    );
  }
  
  private String displayInvokationContext(InvocationContext ic) {
    String beanName = ic.getTarget().getClass().getSimpleName();
    beanName = simpleNameCleaned(beanName);
    String methodName = ic.getMethod().getName();
    return String.format("entering %s -> %s()", beanName, methodName);
  }
}
