package model;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import util.ConsoleUtil;
import util.LoggingInterceptor;
import static util.StringUtil.simpleNameCleaned;

//@Named("book")
@RequestScoped
@Interceptors(LoggingInterceptor.class)
public class Book {

//  private Long id;
  private String title;
  private String publisher;
  private String isbn;
  private String author;
  private String date;
  
  @Inject private BookService bookService;

  public Book () {     
    ConsoleUtil.format("%s -> Constructor 1", simpleNameCleaned(this));
  }
  
//  public Book(Long id, String title, String publisher, String isbn, String author, String date) {
//    this(title, publisher, isbn, author, date);
//    this.id = id;
//  }
//    
  public Book(String title, String publisher, String isbn, String author, String date) {
    this.title = title;
    this.publisher = publisher;
    this.isbn = isbn;
    this.author = author;
    this.date = date;
    
    ConsoleUtil.format("%s -> Constructor 3", simpleNameCleaned(this));
  }
  
  @PostConstruct
  private void init() {
      ConsoleUtil.format("%s -> @PostConstruct: %s", simpleNameCleaned(this), toString() );
  }

  public String getTitle() {
    return title;
  }

  public String getPublisher() {
    return publisher;
  }

  public String getIsbn() {
    return isbn;
  }

  public String getAuthor() {
    return author;
  }

  public String getDate() {
    return date;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public void setDate(String date) {
    this.date = date;
  }
  
  public void save() {
    bookService.add(this);
  }

  @Override
  public String toString() {
    return "" + getAuthor();
  }
  
  
}
