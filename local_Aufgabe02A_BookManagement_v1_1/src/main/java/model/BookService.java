package model;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.interceptor.Interceptors;
import util.ConsoleUtil;
import util.LoggingInterceptor;
import static util.StringUtil.simpleNameCleaned;

@ApplicationScoped
@Interceptors(LoggingInterceptor.class)
public class BookService {

  private static final String FORMAT = "%35s %25s %25s %30s %15s <br/>";
  private final List<Book> bookList;

  
  public BookService() {    
    ConsoleUtil.format("%s -> Constructor", simpleNameCleaned(this));
    bookList = new ArrayList<>();
  }

  public void add(Book book) {
    ConsoleUtil.format("%s -> add() : %s", simpleNameCleaned(this), book.toString());
    bookList.add(book);
  }
  
  public String displayBookList() {
    ConsoleUtil.format("%s -> displayBookList()", simpleNameCleaned(this));
        
    StringBuilder output = new StringBuilder(headerString());
    for (int i = 0; i < bookList.size(); i++) {
      Book book = bookList.get(i);
      output.append(
              String.format(FORMAT,
                book.getTitle(),
                book.getPublisher(),
                book.getIsbn(),
                book.getAuthor(), 
                book.getDate()
              ));      
    }
    System.out.println("bookList = " + output);
    return output.toString();
  }

  private static String headerString() {
    return String.format(FORMAT, 
            "Title", 
            "Publisher", 
            "ISBN", 
            "Author", 
            "Date");
  }


  
}
