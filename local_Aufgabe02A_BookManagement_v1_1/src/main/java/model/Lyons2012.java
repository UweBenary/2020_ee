package model;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Lyons2012 extends Book {

  private static final String TITLE = "OCEJWCD Study Companion";
  private static final String PUBLISHER = "Garner Press";
  private static final String ISBN = "978-0-9551-6034-9";
  private static final String AUTHOR = "Charles Lyons";
  private static final String DATE = "15. August 2012";
  
  public Lyons2012() {
    super(TITLE, PUBLISHER, ISBN, AUTHOR, DATE);
  }

  
}
