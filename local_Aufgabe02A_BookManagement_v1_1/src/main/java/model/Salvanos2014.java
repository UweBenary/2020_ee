package model;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Salvanos2014 extends Book {

  private static final String TITLE = "Professionell entwickeln mit Java EE 7";
  private static final String PUBLISHER = "Rheinwerk Computing";
  private static final String ISBN = "978-3-8362-2004-0";
  private static final String AUTHOR = "Alexander Salvanos";
  private static final String DATE = "2014";
  
  public Salvanos2014() {
    super(TITLE, PUBLISHER, ISBN, AUTHOR, DATE);
  }
}
