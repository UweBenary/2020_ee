package model;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Ullenboom2011 extends Book {

  private static final String TITLE = "Java ist auch eine Insel";
  private static final String PUBLISHER = "Rheinwerk Computing";
  private static final String ISBN = "978-3-8362-1802-3";
  private static final String AUTHOR = "Christian Ullenboom";
  private static final String DATE = "2011";
  
  public Ullenboom2011() {
    super(TITLE, PUBLISHER, ISBN, AUTHOR, DATE);
  }
}
