package controller;

import java.io.IOException;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Book;
import model.BookService;
import util.ConsoleUtil;
import util.LoggingInterceptor;
import static util.StringUtil.simpleNameCleaned;

@WebServlet("/MC")
@Interceptors(LoggingInterceptor.class)
public class MainController extends HttpServlet {
    @Inject
    BookService bookService;

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    ConsoleUtil.format("%s -> doPost()", simpleNameCleaned(this));
    processRequest(request, response);
  }
  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
   ConsoleUtil.format("%s -> doGet()", simpleNameCleaned(this));
   processRequest(request, response);     
  }

  private void bookList(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    ConsoleUtil.format("%s -> bookList()", simpleNameCleaned(this));
    
    String output = bookService.displayBookList();
    
    request.setAttribute("bookList", output);
    request.getRequestDispatcher("bookList.jsp").forward(request, response);
  }

  private void createBook(HttpServletRequest request, HttpServletResponse response) 
                   throws ServletException, IOException {
    ConsoleUtil.format("%s -> bookCreate()", simpleNameCleaned(this));
    
    String todo = request.getParameter("defaultBook");
    switch (todo) {
      case "Ullenboom, 2011":
        bookService.add(new model.Ullenboom2011()); break;
      case "Salvanos, 2014":
        bookService.add(new model.Salvanos2014()); break;
      case "Lyons, 2012":
        bookService.add(new model.Lyons2012()); break;
      default:
        addNewBook(request, response);
    }
    
    bookList(request, response);    
  }

  private void addNewBook(HttpServletRequest request, HttpServletResponse response) {
    
    ConsoleUtil.format("%s -> addNewBook()", simpleNameCleaned(this));
    
    String title = request.getParameter("title");
    String publisher = request.getParameter("publisher");
    String isbn = request.getParameter("isbn");
    String author = request.getParameter("author");
    String date = request.getParameter("date");
    Book book = new Book(title, publisher, isbn, author, date);
    bookService.add(book);
  }

  private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  ConsoleUtil.format("%s -> processRequest()", simpleNameCleaned(this));
    
    String todo = request.getParameter("todo");
    switch (todo) {
      case "bookList":
        bookList(request, response); break;
      case "bookCreate":
        createBook(request, response); break;
      default:
        throw new AssertionError();
    }
   }
}
