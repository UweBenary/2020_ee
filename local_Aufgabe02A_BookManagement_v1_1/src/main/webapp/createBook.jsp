<%-- 
    Document   : bookCreate
    Created on : 06.05.2020, 21:10:30
    Author     : Your Name <yourname@comcave.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Add new book</title>
  </head>
  <body>
    <jsp:include page="header.jsp" />

    <h1>Create new book entry</h1>
    <form target="MC" >
 
  <ul>
    <li>
      <label for="title">Title</label> 
      <input id="title" type="text" name="title" >
    </li>
    <li>
      <label for="publisher">Name</label> 
      <input id="publisher" type="text" name="publisher">
    </li>
    <li>
      <label for="isbn">ISBN</label> 
      <input id="isbn" type="text" name="isbn" >
    </li>
    <li>
      <label for="author">Author</label> 
      <input id="author" type="text" name="author">
    </li>
    <li>
      <label for="date">Title</label> 
      <input id="date" type="text" name="date" >
    </li>
    <li>
      <button type="submit" name="defaultBook" value="Ullenboom, 2011">Ullenboom, 2011</button>
      <button type="submit" name="defaultBook" value="Salvanos, 2014">Salvanos, 2014</button>
      <button type="submit" name="defaultBook" value="Lyons, 2012">Lyons, 2012</button>
      <button type="reset" >Reset</button>
      <button type="submit" name="defaultBook" value="newBook">Save</button>
    </li>
  </ul>
      <input type="hidden" name="todo" value="createBook"/>
    </form>
    
 

  </body>
</html>
