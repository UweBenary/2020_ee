<%-- 
    Document   : index
    Created on : 06.05.2020, 21:06:00
    Author     : Your Name <yourname@comcave.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Book list</title>
  </head>
  <body>
    <jsp:include page="header.jsp" />
    <h1>Book list</h1>
    <%= request.getAttribute("bookList") %>
  </body>
</html>
