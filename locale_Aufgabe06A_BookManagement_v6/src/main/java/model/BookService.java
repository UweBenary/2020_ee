package model;


import java.sql.SQLException;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;



//@ApplicationScoped   // CDI Bean
@RequestScoped
@Transactional
public class BookService {
  
  @PersistenceContext(unitName = "myPU")
  private EntityManager em;

  public BookService() {
  }

  
  public void save(Book book) throws Exception {
//    if (true) {
//      throw new RuntimeException(new SQLException("Some issue"));
//    }
    em.persist(book);
  }

  public List<Book> getBookList() {
    String jpqlString = "SELECT b FROM Book b";
    List<Book> bookList = em.createQuery(jpqlString,Book.class).getResultList();
    return bookList;
  }

  public Book getBookById(long id) {
    Book book = em.find(Book.class, id);
    return book;
  }

  public void update(Book book) {
    em.merge(book);
  }

  public Book delete(long id) {
    Book book = getBookById(id);
    em.remove(book);
    return book;
  }
  
}
