package controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.Interceptors;
import model.Book;
import model.BookService;
import util.ConsoleUtil;
import util.JsfUtil;

@RequestScoped
//@SessionScoped
@Named  // (für die Erreichbarkeit in JSF Facelets)
@Interceptors(LoggingInterceptor.class)
public class BookController implements Serializable {

  @Inject
  private BookService bookService;

  private Book book;

  private List<Book> bookList;

  public BookController() {
    book = new Book();
  }

  public Book getBook() {
    return book;
  }

  public void save() throws IOException {
    ConsoleUtil.print("BookController.save()");
    try {
      bookService.save(book);
      JsfUtil.addSuccessMessage("book.create.success.message", book.getTitle());
    } catch (Exception e) {
      JsfUtil.addErrorMessage(e, "book.create.error.message", book.getTitle());
    }
  }

  public List<Book> getBookList() {
    ConsoleUtil.print("BookController.getBookList()");
    // Um Mehrfach-Aufrufe vom BookService zu vermeiden !!!
//    if (bookList == null) {
//      bookList = bookService.getBookList();
//    }
    bookList = bookService.getBookList();
    return bookList;
  }

  public void delete(long id) {
    try {
      this.book = bookService.delete(id);
      JsfUtil.addSuccessMessage("book.delete.success.message", book.getTitle());
    } catch (Exception e) {
      JsfUtil.addErrorMessage(e, "book.delete.error.message", book.getTitle());
    }
  }

  public String edit(long id) {
    book = bookService.getBookById(id);
    return "bookUpdate"; // bookUpdate.xhtml
  }

  public String update() {
//    ConsoleUtil.format("BookController.udpate(%d)", book.getId());

    try {
      bookService.update(book);
      JsfUtil.addSuccessMessage("book.update.success.message", book.getTitle());
    } catch (Exception e) {
      JsfUtil.addErrorMessage(e, "book.update.error.message", book.getTitle());
    }
    return "bookUpdate";
  }

  public String log(String msg) {
    ConsoleUtil.print(msg);
    return msg;
  }

}
