package rest;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.business.PersonService;
import model.business.csvImport.util.ColorCode;
import model.persistence.Person;

//@Stateless
@RequestScoped
@Path("/persons")
public class PersonResource implements Serializable {

  @Inject
  private PersonService personService;

  public PersonResource() {
  }

  public PersonResource(PersonService personService) {
    this.personService = personService;
  }
  
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Person> getAllPersons() {
    return personService.getPersonList();
  }

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Person getPersonById(@PathParam("id") long id) {
    return personService.getPersonById(id);
  }

  @GET
  @Path("/color/{color}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Person> getAllPersonsOfColor(@PathParam("color") String color) {
    if (color.matches("\\d+")) {
      color = ColorCode.getColorByIndex(color);

    }
    return personService.getAllPersonsOfColor(color);
  }

  @POST
  @Consumes({MediaType.APPLICATION_JSON})
  public void create(Person person) {
    personService.create(person);
  }

  @PUT
  @Path("/{id}")
  @Consumes({MediaType.APPLICATION_JSON})
  public void edit(@PathParam("id") long id, Person person) {
    person.setId(id);
    personService.update(person);
  }

  @DELETE
  @Path("/{id}")
  public void remove(@PathParam("id") long id) {
    personService.remove(id);
  }

}
