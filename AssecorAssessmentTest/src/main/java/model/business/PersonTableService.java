package model.business;

import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import model.persistence.Person;
import model.persistence.internalSchema.PersonTable;

@Stateless
public class PersonTableService implements PersonService, Serializable {

  @EJB
  private PersonTable personTable;

  @Override
  public void create(Person person) {
    personTable.add(person);
  }

  @Override
  public void update(Person person) {
    personTable.replace(person);
  }

  @Override
  public void remove(long id) {
    Person person = new Person(id);
    personTable.delete(person);
  }

  @Override
  public List<Person> getPersonList() {
    return personTable.getAll();
  }

  @Override
  public Person getPersonById(long id) {
    Person person = new Person(id);

    Person found = getPersonList().stream()
            .unordered()
            .parallel()
            .filter(person::equals)
            .findFirst()
            .orElse(null);
    return found;
  }

  @Override
  public List<Person> getAllPersonsOfColor(String color) {
    Predicate<Person> hasEqualColor = p -> p.getColor().equals(color);
    List<Person> found = getPersonList().stream()
            .unordered()
            .parallel()
            .filter(hasEqualColor)
            .sorted()
            .collect(Collectors.toList());
    return found;
  }
}
