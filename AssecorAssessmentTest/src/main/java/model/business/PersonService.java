package model.business;

import java.util.List;
import javax.ejb.Local;
import javax.enterprise.context.Dependent;
import model.persistence.Person;

@Local
public interface PersonService {
  
//  PersonService newInstance();

  void create(Person person);

  void update(Person person);

  void remove(long id);

  Person getPersonById(long id);

  List<Person> getPersonList();

  List<Person> getAllPersonsOfColor(String color);

}
