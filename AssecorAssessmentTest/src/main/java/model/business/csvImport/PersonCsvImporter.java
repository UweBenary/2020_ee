package model.business.csvImport;

import model.business.csvImport.util.StringPersonConverter;
import java.io.BufferedReader;
//import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import model.persistence.Person;
import model.persistence.internalSchema.IndexGenerator;

@RequestScoped
public class PersonCsvImporter {

  @Inject
  @CsvResource
  private String csvFile;

//  @Inject
//  private StringPersonConverter converter;

  @EJB
  private IndexGenerator indexGenerator;

  private final Collection<Person> persons = new ArrayList<>();

  private final static String RESET = "";
  
  @Produces
  public Collection<Person> readPersonCsv() {

    try (   InputStream input = PersonCsvImporter.class.getResourceAsStream(csvFile);
            InputStreamReader isr = new InputStreamReader(input, "UTF8");
            BufferedReader in = new BufferedReader(isr) ) {

      String inStr;
      String cumulativeStr = RESET;
      int lineCount = 0;

      while ((inStr = in.readLine()) != null) {

        cumulativeStr += inStr;
        
        String[] splitStr = cumulativeStr.split(", ");
        if (splitStr.length > 4) {
          throw new IOException("Number of elements read > 4 @line: " + lineCount + " -> " + cumulativeStr);
        }
        if (splitStr.length == 4) {
          addPerson(splitStr);
          cumulativeStr = RESET;
        }

      }
    } catch (FileNotFoundException ex) {
      System.err.println("File not found error: " + ex);
    } catch (IOException ex) {
      System.err.println("I/O error: " + ex);
    }
    return persons;
  }

  private void addPerson(final String[] splitStr) {
//    Person person = converter.convert(splitStr);
    Person person = StringPersonConverter.convert(splitStr);
    person.setId(indexGenerator.newIndex());
    persons.add(person);
  }

}
