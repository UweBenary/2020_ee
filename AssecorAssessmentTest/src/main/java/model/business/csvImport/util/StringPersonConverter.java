package model.business.csvImport.util;

import model.persistence.Person;

public class StringPersonConverter {

  public static Person convert(String[] strings) {
    Person person = new Person();
    person.setLastname(strings[0].trim());
    person.setName(strings[1].trim());

    final int indexOf = strings[2].indexOf(" ");
    person.setZipcode(strings[2].substring(0, indexOf));
    person.setCity(strings[2].substring(indexOf + 1).trim());

    String colorIdx = strings[3].trim();
    String color = ColorCode.getColorByIndex(colorIdx);
    person.setColor(color);

    return person;
  }
}
