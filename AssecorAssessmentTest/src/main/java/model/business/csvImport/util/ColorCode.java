package model.business.csvImport.util;

import java.util.ArrayList;
import java.util.List;

public class ColorCode {

  private static final List<String> COLOR_MAP;

  static {
    COLOR_MAP = new ArrayList<>();
    COLOR_MAP.add(""); // to match index starting at 1
    COLOR_MAP.add("blau");
    COLOR_MAP.add("grün");
    COLOR_MAP.add("violett");
    COLOR_MAP.add("rot");
    COLOR_MAP.add("gelb");
    COLOR_MAP.add("türkis");
    COLOR_MAP.add("weiß");
  }

  public static String getColorByIndex(final String index)
          throws NumberFormatException, IndexOutOfBoundsException {
    final int idx = Integer.valueOf(index);
    return getColorByIndex(idx);
  }
  
  public static String getColorByIndex(final int index)
          throws NumberFormatException, IndexOutOfBoundsException {
    if (index < 1) {
      throw new IndexOutOfBoundsException();
    }
    return COLOR_MAP.get(index);
  }

  public static int getIndexOfColor(String color) {
    final int idx = COLOR_MAP.indexOf(color);
    if (idx < 1) {
      throw new IllegalArgumentException("Color unknown.");
    }
    return idx;
  }

}
