package model.business.csvImport;

import model.business.csvImport.CsvResource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

@RequestScoped
public class CsvResourceProvider {
  
  @Produces @CsvResource
   public final static String CSV_FILE = "/sample-input.csv";

}
