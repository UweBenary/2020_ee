package model.business;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.persistence.Person;

@Stateless
@Alternative
public class PersonDBService implements PersonService, Serializable {

//  @PersistenceContext(unitName = "assecorAssessmentTest_PU")
  @PersistenceContext(unitName = "assecorAssessmentTest_GF_PU")
  private EntityManager em;
  
  @Inject
  private Collection<Person> persons;
  

  @PostConstruct
  private void loadPersonCsv() {
    System.out.println("==> PersonDBService is used.");    
    persons.forEach(this::create);
  }
  
  @Override
  public void create(Person person) {
    em.persist(person);
  }

  @Override
  public void update(Person person) {
  em.merge(person);
  }

  @Override
  public void remove(long id) {
    Person person = getPersonById(id);
    em.remove(person);
 }

  @Override
  public Person getPersonById(long id) {
    return em.find(Person.class, id);
  }

  @Override
  public List<Person> getPersonList() {
    String jpqlString = "SELECT p FROM Person p";
    List<Person> personsList = em.createQuery(jpqlString, Person.class).getResultList();
    Collections.sort(personsList);
    return personsList;
  }

  @Override
  public List<Person> getAllPersonsOfColor(String color) {
    String jpqlString = "SELECT p FROM Person p WHERE p.color LIKE :colour";
    List<Person> personsList = em.createQuery(jpqlString, Person.class)
            .setParameter("colour", color)
            .getResultList();
    Collections.sort(personsList);
    return personsList;
  }

}
