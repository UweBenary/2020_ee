package model.persistence.internalSchema;

import java.io.Serializable;
import javax.ejb.Singleton;

@Singleton
public class IndexGenerator implements Serializable {
  
  private long index;
  
  public long newIndex() {
    return ++index;
  }

}
