package model.persistence.internalSchema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import model.persistence.Person;

@Singleton
public class PersonTable implements Serializable {

  @Inject
  private Collection<Person> persons;

  @EJB
  private IndexGenerator indexGenerator;

  public void add(Person person) {
    person.setId(indexGenerator.newIndex());
    persons.add(person);
  }

  public void delete(Person person) {
    persons.remove(person); // based on Person::equals compares id
  }

  public void replace(Person person) {
    delete(person); // based on equals() comparing id
    add(person);
  }

  public List<Person> getAll() {
    List<Person> personList = new ArrayList<>(persons);
    Collections.sort(personList);
    return personList;
  }

}
