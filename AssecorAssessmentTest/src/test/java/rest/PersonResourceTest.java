package rest;

//import javax.ejb.embeddable.EJBContainer;
//import org.glassfish.ejb.embedded.EJBContainerImpl;
//import org.jboss.weld.junit5.WeldInitiator;
//import org.jboss.weld.junit5.WeldSetup;
//import org.jboss.weld.junit5.EnableWeld;
//import org.jboss.weld.environment.se.Weld;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test; // https://stackoverflow.com/questions/61800642/the-junit-test-fails-in-netbeans-11-3-ide-could-not-find-or-load-main-class
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import model.business.PersonService;
import model.persistence.Person;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

//@EnableWeld
public class PersonResourceTest {

  private PersonResource resource;
  
  @Mock
  private PersonService service;
  
  @Rule public MockitoRule rule = MockitoJUnit.rule();
  
  @Before
  public void init() {
    resource = new PersonResource(service);
  }

//  public PersonResourceTest() {
//    service = mock(PersonService.class);
//    resource = new PersonResource(service);
//  }

  @Test
  public void testGetAllPersons() {
    final List<Person> expected = new ArrayList<>();
    when(service.getPersonList()).thenReturn(expected);

    List<Person> actual = resource.getAllPersons();
    assertEquals(expected, actual);
  }

  @Test
  public void testGetPersonById() {
    final long id = 1;
    final Person expected = new Person(id);
    when(service.getPersonById(id)).thenReturn(expected);

    Person actual = resource.getPersonById(id);
    assertEquals(expected, actual);
    System.out.println("getPersonById");
  }

  @Test
  public void testGetAllPersonsOfColor() {
    final String color = "blau";
    final List<Person> expected = new ArrayList<>();
    when(service.getAllPersonsOfColor(color)).thenReturn(expected);

    List<Person> actual = resource.getAllPersonsOfColor(color);
    assertEquals(expected, actual);
  }

  @Test
  public void testCreate() {
    final long id = 1;
    final Person expected = new Person(id);

    resource.create(expected);
    verify(service).create(expected);
  }

  @Test
  public void testEdit() {
    final long id = 1;
    final Person expected = new Person(id);

    resource.edit(id, expected);
    verify(service).update(expected);
  }

  @Test
  public void testRemove() {
    final long id = 1;

    resource.remove(id);
    verify(service).remove(id);
  }
}

/* 
    org.jboss.weld
    
    not working: fail to get instance of weld for testing PersonResource with DI of a PersonServiceMock
 */
//        @WeldSetup 
//    private WeldInitiator weld = WeldInitiator.performDefaultDiscovery();
//        
//    final Weld weldBase = WeldInitiator.createWeld();
//     final Weld weldBase = WeldInitiator.enableDiscovery();
//     final Weld weldBase =WeldInitiator.createWeld()
//            .addBeanClasses(
//            PersonResource.class, 
//            PersonResourceTest.class
//            );
//                    
//    @WeldSetup
//    public WeldInitiator weld = WeldInitiator.
//            from(weldBase)
//            .activate(RequestScoped.class).build();
// 
//    @Test
//    public void testGetAllPersons() {
//        System.out.println("===>  getAllPersons");
//        
//        System.out.println("-> " + weld.select(PersonResource.class).get().getAllPersons());
//        assertTrue(true);
//    }
