package model;


import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class IdGenerator {
  
  private long count;

  public IdGenerator() {
    count = 0L;
  }
  
  @Produces
  public Long getId() {
    count++;
    return count;    
  }
  
}
