package controller;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import model.Book;
import model.BookService;

@Named
@RequestScoped
public class BookController implements Serializable {
  
  @Inject
  private BookService bookService;
  private Book book;

  public BookController() {
    book = new Book();
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public void save() {
    bookService.addBook(book);
    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, 
            "Book added", "Book [" + book + "] has been added");
    FacesContext.getCurrentInstance().addMessage(null, message);
  }
  
  public List<Book> getBookList() {
    return bookService.getBookList();
  }
  
  public void deleteBook() {
    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, 
            "Book added", "Book [" + book + "] has been deleted");
    bookService.deleteBook(book.getId());
    FacesContext.getCurrentInstance().addMessage(null, message);
  }
    
  public String updateBook(String id) {
    bookService.updateBook(id, book);
    return "bookList"; //.xhtml
  }
  
}
