package model;

import aop.BenchmarkInterceptor;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import javax.enterprise.context.ApplicationScoped;
import javax.interceptor.Interceptors;

@ApplicationScoped
@Interceptors(BenchmarkInterceptor.class)
public class BookService {

  private List<Book> bookList;

  public BookService() {
    bookList = new ArrayList<>();
  }

  
  public void addBook(Book book) {
    bookList.add(book);
  }
  
  public Book getBook(String id) {
    return findBook(id).orElse(null);
  }

  public List<Book> getBookList() {
    return bookList;
  }

  public void updateBook(String id, Book update) {
    Book book = getBook(id);
    book.setAuthor( update.getAuthor() );
    book.setDate( update.getDate() );
    book.setIsbn( update.getIsbn() );
    book.setPublisher( update.getPublisher() );
    book.setTitle( update.getTitle() );
  }
  
  public void deleteBook(long id) {
    deleteBook("" + id);
  }
  public void deleteBook(String id) {
    final Optional<Book> findFirst = findBook(id);
    if (findFirst.isPresent()) {
         bookList.remove(findFirst.get());
    }
  }


  private Optional<Book> findBook(final String id) {
   return bookList.stream()
            .unordered()
            .parallel()
            .filter( new Predicate<Book> () {
                      @Override
                      public boolean test(Book book) {return book.getId().equals(Long.valueOf(id)); }
                    })
            .findFirst();
  }
  
  
}
