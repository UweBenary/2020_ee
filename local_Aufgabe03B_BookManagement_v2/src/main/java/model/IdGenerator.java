package model;

import aop.BenchmarkInterceptor;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import javax.interceptor.Interceptors;

@ApplicationScoped
@Interceptors(BenchmarkInterceptor.class)
public class IdGenerator {
  
  private long count;

  public IdGenerator() {
    count = 0L;
  }
  
  @Produces
  public Long getId() {
    count++;
    System.out.println("count: " + count);
    return count;
    
  }
  
}
