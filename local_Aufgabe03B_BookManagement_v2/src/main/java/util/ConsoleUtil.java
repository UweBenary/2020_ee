package util;

public class ConsoleUtil {
  
  private static final String LEADING_STRING = "=======> ";

  public static void format(String stringFormat, Object... objects) {
    System.out.format(LEADING_STRING + stringFormat, objects);
  }
}
