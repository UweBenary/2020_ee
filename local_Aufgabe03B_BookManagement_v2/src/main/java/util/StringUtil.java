package util;
public class StringUtil {
  
  public static String simpleNameCleaned(Object bean) {
    return cleaned(bean.getClass().getSimpleName());
  }

  public static String cleaned(String beanName) {
    return beanName.contains("$") ? beanName.substring(0, beanName.indexOf("$")) + " (Proxy)" : beanName;
  }
}
