package controller;

import java.io.IOException;
import java.util.List;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Book;
import model.BookService;

@WebServlet("/MS")
public class MyServlet extends HttpServlet {
  
  @Inject
  private BookService bookService;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
 
    String todo = request.getParameter("todo");
    
    if (todo == null) {
      todo = "";
    }
    
    switch (todo) {
      case "bookList":
        showBooklist(request, response); break;
      case "createBook":
        createBook(request, response); break;
      case "deleteBook":
        deleteBook(request, response); break;
      case "editBook":
        editBook(request, response); break;
      case "saveEdit":
        saveEdit(request, response); break;
      default:
        nothingTodo(request, response);          
    }
    
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
  
  }

  private void showBooklist(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    String attributeName = "bookList";
    List<Book> attributeValue = bookService.getBookList();
    request.setAttribute(attributeName, attributeValue);
    request.getRequestDispatcher("bookList.jsp").forward(request, response);
  }

  private void nothingTodo(HttpServletRequest request, HttpServletResponse response) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  private void createBook(HttpServletRequest request, HttpServletResponse response) 
                  throws ServletException, IOException {
    // Controller tasks
    String title = request.getParameter("title");
    String publisher = request.getParameter("publisher");
    String isbn = request.getParameter("isbn");
    String author = request.getParameter("author");
    String date = request.getParameter("date");

    Book book = new Book(title, publisher, isbn, author, date);

//    Book book2 = CDI.current().select(Book.class).get();
     // Model 
    bookService.addBook(book);
    
    // View
    request.setAttribute("info", "Book [" + title + "] is added...");
    request.getRequestDispatcher("createBook.jsp").forward(request, response);
  }

  private void deleteBook(HttpServletRequest request, HttpServletResponse response) 
                  throws ServletException, IOException {
    // controller
    String bookId = request.getParameter("id");
    // model
    Book deletedBook = bookService.getBook(bookId);
    bookService.deleteBook(bookId);
    // view
    request.setAttribute("info", "Book [" + deletedBook.toString() + "] is deleted...");
    request.getRequestDispatcher("createBook.jsp").forward(request, response);
    
  }

  private void editBook(HttpServletRequest request, HttpServletResponse response) 
                  throws ServletException, IOException {
     // controller
    String bookId = request.getParameter("id");
    // model
    Book book = bookService.getBook(bookId);
    
    // view
    request.setAttribute("book", book);
    request.getRequestDispatcher("editBook.jsp").forward(request, response);
  }

  private void saveEdit(HttpServletRequest request, HttpServletResponse response) 
                  throws ServletException, IOException {
    // Controller tasks
    String id = request.getParameter("id");
    String title = request.getParameter("title");
    String publisher = request.getParameter("publisher");
    String isbn = request.getParameter("isbn");
    String author = request.getParameter("author");
    String date = request.getParameter("date");

    Book book = new Book(title, publisher, isbn, author, date);
     // Model 
    bookService.updateBook(id, book);
    
    // View
    request.setAttribute("info", "Book #" + id + " is edited...");
    request.getRequestDispatcher("editBook.jsp").forward(request, response);
  }

  
}
