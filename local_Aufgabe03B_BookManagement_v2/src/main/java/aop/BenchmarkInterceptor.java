package aop;

import javax.annotation.Priority;
import javax.interceptor.AroundConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import util.ConsoleUtil;
import util.StringUtil;
import static util.StringUtil.simpleNameCleaned;

@Interceptor
@Priority(200)
public class BenchmarkInterceptor {

  public BenchmarkInterceptor() {
    init();
  }

  private void init() {
    ConsoleUtil.format("%s (Constructor)", simpleNameCleaned(this));
  }
    
//  @AroundConstruct
//  private void interceptConstructor(InvocationContext ic) throws Exception {
//     ConsoleUtil.format("%s (@AroundConstruct)", simpleNameCleaned(this));
//    ic.proceed();
//  }
  
  @AroundInvoke
  private Object intercept(InvocationContext ic) throws Exception {
    long t0 = System.nanoTime();
    Object obj = ic.proceed();
    long t1 = System.nanoTime();
    
    doBenchmarking(t1, t0, ic);
    return obj;
  }

  private void doBenchmarking(long t1, long t0, InvocationContext ic) {
    String benchmark = String.format("Execution Time: %.3f ms", (t1 - t0) / 1.0e6);
    ConsoleUtil.format("%s ==> %s -> %s",
            simpleNameCleaned(this),
            displayInvokationContext(ic),
            benchmark
    );
  }
//  Ausgabe der Konsole: Fehlerhaft!!!, warum String
//INFORMATION:   =======> BenchmarkInterceptor (Constructor)
//INFORMATION:   =======> BenchmarkInterceptor ==> entering String.addBook() -> Execution Time: 0,095 ms
//INFORMATION:   =======> BenchmarkInterceptor ==> entering String.getBookList() -> Execution Time: 0,019 ms  
  
  private String displayInvokationContext(InvocationContext ic) {
    String resultString;
    try {
      String beanName = ic.getTarget().getClass().getSimpleName();
      beanName = StringUtil.simpleNameCleaned(beanName);
      String methodName = ic.getMethod().getName();
      resultString = String.format("entering %s.%s()", beanName, methodName);
    } catch (Exception e) {
      resultString = "";
    }
    return resultString;
  }
}
