package aop;

import java.io.IOException;
import javax.interceptor.InvocationContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import util.ConsoleUtil;
import static util.StringUtil.simpleNameCleaned;

@WebFilter(filterName = "MF", urlPatterns = "*.jsp", servletNames = "MS")
public class BenchmarkFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    long t0 = System.nanoTime();
    chain.doFilter(request, response);
    long t1 = System.nanoTime();
    
    doBenchmarking(t1, t0);
  }

  @Override
  public void destroy() {
  }
  
  private void doBenchmarking(long t1, long t0) {
    String benchmark = String.format("Execution Time: %.3f ms", (t1 - t0) / 1.0e6);
    ConsoleUtil.format("%s -> %s",
            simpleNameCleaned(this),
            benchmark
    );
  }
}
