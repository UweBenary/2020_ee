<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create a new Book</title>
    
    <link rel="stylesheet" href="main.css" />
    
    <script type="text/javascript">
      function fillFormUllenboom() {
        document.getElementsByName("title")[0].value = 'Java ist auch eine Insel';
        document.getElementsByName("publisher")[0].value = 'Rheinwerk Computing';
        document.getElementsByName("isbn")[0].value = '978-3-8362-1802-3';
        document.getElementsByName("author")[0].value = 'Christian Ullenboom';
        document.getElementsByName("date")[0].value = '2011';
      }
  
      function fillFormSalvanos() {
        document.getElementsByName("title")[0].value = 'Professionell entwickeln mit Java EE 7';
        document.getElementsByName("publisher")[0].value = 'Rheinwerk Computing';
        document.getElementsByName("isbn")[0].value = '978-3-8362-2004-0';
        document.getElementsByName("author")[0].value = 'Alexander Salvanos';
        document.getElementsByName("date")[0].value = '2014';
      }
      
      function fillFormLyons() {
        document.getElementsByName("title")[0].value = 'OCEJWCD Study Companion';
        document.getElementsByName("publisher")[0].value = 'Garner Press';
        document.getElementsByName("isbn")[0].value = '978-0-9551-6034-9';
        document.getElementsByName("author")[0].value = 'Charles Lyons';
        document.getElementsByName("date")[0].value = '15. August 2012';
      }
      
    </script>
      
  </head>
  <body>
    <jsp:include page="header.jsp" />
    <h1>Create a new Book</h1>
    
    
    
    <form action="MS" method="GET">
      <table>
        <tr>
          <td>
            Title:
          </td>
          <td>
            <input type="text" name="title" required="required" value="${book.title}"></td>         
          </td>
        </tr>
        <tr>
          <td>
            Publisher:
          </td>
          <td>
            <input type="text" name="publisher"  value="${book.publisher}" />         
          </td>
        </tr>
        <tr>
          <td>
            ISBN:
          </td>
          <td>
            <input type="text" name="isbn"  value="${book.isbn}" />         
          </td>
        </tr>
        <tr>
          <td>
            Author:
          </td>
          <td>
            <input type="text" name="author"  value="${book.author}" />         
          </td>
        </tr>
        <tr>
          <td>
            Date:
          </td>
          <td>
            <input type="text" name="date"  value="${book.date}" />         
          </td>
        </tr>
      </table>
      
      <br/><br/>
      
      <table>
        <tr>
          <td colspan="2">
            <input type="button" value="Ullenboom, 2011" onclick="fillFormUllenboom()" />
            <input type="button" value="Salvanos, 2014" onclick="fillFormSalvanos()" />
            <input type="button" value="Lyons, 2012" onclick="fillFormLyons()" />
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <input type="reset" value="Reset" />       
            <input type="submit" value="Save" />  
          </td>
        </tr>
      </table>
      
      <input type="hidden" name="id" value="${book.id}"/>
      <input type="hidden" name="todo" value="saveEdit"/>
     
    </form>
    
    <jsp:include page="footer.jsp"/>
    
  </body>
</html>
