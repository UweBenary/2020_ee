<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Book list</title>
  </head>
  <body>
    <jsp:include page="header.jsp" />
    
    <h1>Book list</h1>

    <table>
      <tr>
        <th>ID</th>
        <th>Titel</th>
        <th>Verlag</th>
        <th>ISBN</th>
        <th>Author</th>
        <th>Erscheinungsdatum</th>
        
        <th></th>
        <th></th>
      </tr>
      <c:forEach items="${bookList}" var="book">
        <tr>
          <td>${book.id}</td>
          <td>${book.title}</td>
          <td>${book.publisher}</td>
          <td>${book.isbn}</td>
          <td>${book.author}</td>
          <td>${book.date}</td>

          <td><a href="MC?todo=editBook&id=${book.id}">edit</a></td>
          <td><a href="MC?todo=deleteBook&id=${book.id}">delete</a></td>
        </tr>
      </c:forEach>
    </table>
    
    <jsp:include page="footer.jsp"/>
  </body>
</html>