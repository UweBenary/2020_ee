
<%@page import="java.util.Arrays, model.Person, java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>personList</title>
  </head>
  <body>
    <h1>personList</h1>
    <%
      List<Person> personList = Arrays.asList(
              new Person("Peter"),
              new Person("Paul"),
              new Person("Mary")
      );
      request.setAttribute("personList", personList);
    %>
    
    <table>
      <% for (int idx = 0; idx < personList.size(); idx++) {  %>
      <tr>
        <td>
          <%= personList.get(idx) %>
        </td>
      </tr>
      <% } %>
    </table>
              
  </body>
</html>
