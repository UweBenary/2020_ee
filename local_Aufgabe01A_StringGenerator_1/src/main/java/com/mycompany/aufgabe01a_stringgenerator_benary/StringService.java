package com.mycompany.aufgabe01a_stringgenerator_benary;

import java.util.Random;

public class StringService {

  static String getRandomString() {
    StringBuilder sb = new StringBuilder();
    Random rand = new Random();
    int stringLength = rand.nextInt(100 - 30) + 30;
    for (int i = 0; i < stringLength; i++) {
      sb.append(getRandomCharacter()); 
    }
    return sb.toString();
  }
  
  
  private static String getRandomCharacter() {
    final int charFor0 = 48;
    final int charFor9 = 57;
    final int charForA = 65;
    final int charForZ = 90;
    final int charFora = 97;
    final int charForz = 122;
    Random rand = new Random();
    
    int result;
   
    switch(rand.nextInt(3)){
        case 0:
          result = (rand.nextInt(charFor9 +1 - charFor0) + charFor0);
          break; 
        case 1:
          result = (rand.nextInt(charForZ +1 - charForA) + charForA);
          break; 
        case 2:
          result = (rand.nextInt(charForz +1 - charFora) + charFora);
          break; 
        default:
          result = 10000000;
     }
    return "" + (char) result;
  }
      
}
