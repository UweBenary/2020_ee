<%-- 
    Document   : StringOutput
    Created on : 04.05.2020, 12:56:46
    Author     : Your Name <yourname@comcave.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>
  <body>
    <h1>Here is your random string:</h1>
    <%= request.getAttribute("generatedString") %>   
    <br>
    <br>
    <a href="index.html"> home</a>
  </body>
</html>
