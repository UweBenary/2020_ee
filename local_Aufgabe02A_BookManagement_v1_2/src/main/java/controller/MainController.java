package controller;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Book;
import model.BookService;
import model.IdGenerator;

@WebServlet("/MC")
public class MainController extends HttpServlet {

  @Inject
  private BookService bookService;
  @Inject
  private IdGenerator idGenerator;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String todo = request.getParameter("todo");
    
    if (todo == null) {
      todo = "";
    }
    
    switch(todo) {
      case "bookList" :
        bookList(request, response); break;
      case "bookCreate" :
        createBook(request, response); break;
      case "bookEdit" :
        bookEdit(request, response); break;
      case "bookSaveEdit" :
        bookSaveEdit(request, response); break;
      case "bookDelete" :
        bookDelete(request, response); break;
      default:
        todoUnknown(request, response);
    }
      
      
  }

  private void createBook(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    // Controller tasks
    String title = request.getParameter("title");
    String publisher = request.getParameter("publisher");
    String isbn = request.getParameter("isbn");
    String author = request.getParameter("author");
    String date = request.getParameter("date");
//    Book book = new Book(title, publisher, isbn, author, date);
    
    Long id = idGenerator.getId();
    Book book = new Book(id, title, publisher, isbn, author, date);
    // Model 
    bookService.add(book);
    
    // View
    request.setAttribute("info", "Book [" + title + "] is added...");
    request.getRequestDispatcher("bookCreate.jsp").forward(request, response);
  }

  private void bookList(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    // Controller tasks
  
    // Model 
    List<Book> output = bookService.getBookList();
    
    // View
    request.setAttribute("bookList", output);
    request.getRequestDispatcher("bookList.jsp").forward(request, response);  
  }

  private void todoUnknown(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  private void bookEdit(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    // Controller tasks
    String id = request.getParameter("id");
    // Model 
    Book book = bookService.getBook(id);
    
    // View
    request.setAttribute("book", book);
    request.getRequestDispatcher("bookEdit.jsp").forward(request, response);  
  }

  private void bookDelete(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    // Controller tasks
    String id = request.getParameter("id");
    // Model 
    
    bookService.deleteBook(id);
    List<Book> output = bookService.getBookList();
    
    // View
    request.setAttribute("bookList", output);
    request.getRequestDispatcher("bookList.jsp").forward(request, response);  
   }

  private void bookSaveEdit(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    // Controller tasks
    String id = request.getParameter("id");
    String title = request.getParameter("title");
    String publisher = request.getParameter("publisher");
    String isbn = request.getParameter("isbn");
    String author = request.getParameter("author");
    String date = request.getParameter("date");
    // Model 
    Book book = bookService.getBook(id);
    book.setTitle(title);
    book.setPublisher(publisher);
    book.setIsbn(isbn);
    book.setAuthor(author);
    book.setDate(date);
    // View
    request.setAttribute("info", "Book #" + id + " is edited...");
    request.getRequestDispatcher("bookEdit.jsp").forward(request, response);
  }

}
