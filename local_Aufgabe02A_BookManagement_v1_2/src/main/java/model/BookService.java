package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class BookService {

  private List<Book> bookList;

  public BookService() {
    bookList = new ArrayList<>();
  }

  
  public void add(Book book) {
    bookList.add(book);
  }

  public List<Book> getBookList() {
    return bookList;
  }

  public void deleteBook(final String id) {
    final Optional<Book> findFirst = findBook(id);
    if (findFirst.isPresent()) {
         bookList.remove(findFirst.get());
    }
  }

  public Book getBook(String id) {
    return findBook(id).orElse(null);
  }

  private Optional<Book> findBook(final String id) {
   return bookList.stream()
            .unordered()
            .parallel()
            .filter( new Predicate<Book> () {
                      @Override
                      public boolean test(Book t) {return t.hasId(id); }
                    })
            .findFirst();
  }
  
  
}
