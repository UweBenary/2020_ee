
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--display line only if info or error--> 
<c:if test="${not empty info or not empty error}">
  <hr/>
</c:if>


<div style="color:green"> ${info} </div>

<div style="color:red"> ${error} </div>
  
