package util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class DateUtil {

  public static String currentTime() {
//    return new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(new Date());
    return DateTimeFormatter.ofPattern("hh:mm:ss").format(LocalTime.now());
  }

  public static LocalDate getRandomDate() {
    Random rd = new Random();
    return LocalDate.of(rd.nextInt(100) + 1920, rd.nextInt(12) + 1, rd.nextInt(28) + 1);
  }
  
}
