package interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import util.ConsoleUtil;
import static util.DateUtil.currentTime;
import util.JsfUtil;
import static util.StringUtil.cleaned;

@Interceptor // optional
public class LoggingInterceptor {

  @AroundInvoke
  public Object log(InvocationContext ic) {
    Object obj = null;
    try {
      String beanName = ic.getTarget().getClass().getSimpleName();
      beanName = cleaned(beanName, false);
      String methodName = ic.getMethod().getName();
      ConsoleUtil.format("%s.%s()", beanName, methodName);
      obj = ic.proceed();
//      ConsoleUtil.format("Exiting %s -> %s()", beanName, methodName);

    } catch (Exception ex) {
      ex.printStackTrace();
      JsfUtil.addErrorMessage(ex, "general.error.message");
    }

    return obj;
  }
}
