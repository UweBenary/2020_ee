package model;

import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import util.ConsoleUtil;

@MessageDriven(
        activationConfig = {
          @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
          @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/news")
        }
)
public class Peter implements MessageListener {


  @Override
  public void onMessage(Message msg) {
    try {
      String message = msg.getBody(String.class);
      ConsoleUtil.printTitle("Peter reads: " + message);
    } catch (JMSException ex) {
      ex.printStackTrace();
    }
    
  }
  
  
  

}
