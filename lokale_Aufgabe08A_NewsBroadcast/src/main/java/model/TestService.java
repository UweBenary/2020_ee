package model;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Topic;

@Stateless
public class TestService {

  @Inject
  @JMSConnectionFactory("jms/myCF")
  private JMSContext jMSContext;
  
  @Resource(lookup = "jms/news")
  private Topic newsTopic;
  
  
  public void sendMessage(String message) {
    final JMSProducer producer = jMSContext.createProducer();
    producer.send(newsTopic, message);
  }
    
}
