package controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import model.TestService;
import util.JsfUtil;

@Named // in JSF Facelet "#{testController}
@RequestScoped
//@Transactional // Only for Transaction Attribute Test
public class TestController {
  
  @Inject
  private TestService testService;
  
  private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }  
  
  public void send() {
    testService.sendMessage(message);
    JsfUtil.addSuccessMessage("Nachricht wurde geschickt");
  }


  
}
