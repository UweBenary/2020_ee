function fillForm1() {
  document.getElementsByName("formId:firstname")[0].value = 'Peter';
  document.getElementsByName("formId:lastname")[0].value = 'Schmidt';
  return false;
}
function fillForm2() {
  document.getElementsByName("formId:firstname")[0].value = 'Hans';
  document.getElementsByName("formId:lastname")[0].value = 'Meyer';
  return false;
}
function fillForm3() {
  document.getElementsByName("formId:firstname")[0].value = 'Thomas';
  document.getElementsByName("formId:lastname")[0].value = 'Arnold';
  return false;
}

function clearForm() {
  document.getElementsByName("formId:firstname")[0].value = '';
  document.getElementsByName("formId:lastname")[0].value = '';
  return false;
}

function defaultCommand() {
//  alert('Hi');
  if (event.keyCode === 13) {
//    alert('Enter pressed!')
    var saveUpdateButton = document.getElementById('formId:saveUpdateButtonId');
    saveUpdateButton.click();
    return false;
  }
}
