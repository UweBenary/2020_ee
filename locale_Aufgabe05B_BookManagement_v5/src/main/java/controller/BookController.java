package controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import model.Book;
import model.BookService;

@Named
@RequestScoped
public class BookController implements Serializable {
  
  @Inject
  private BookService bookService;

  
  private Book book;
  
  private List<Book> bookList;

  public BookController() {
    book = new Book();
  }

  public Book getBook() {
    return book;
  }


  public void save()  throws IOException {
    bookService.addBook(book);
    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, 
            "Book added", "Book [" + book + "] has been added");
    FacesContext.getCurrentInstance().addMessage(null, message);
  }
  
  public List<Book> getBookList() {
    if (bookList == null)
     bookList =  bookService.getBookList();
    return bookList;
  }
  
  public void deleteBook(Long id) {
    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, 
            "Book added", "Book [" + book + "] has been deleted");
    bookService.deleteBook(id);
    FacesContext.getCurrentInstance().addMessage(null, message);
  }
  
  public String editBook(Long id) {
    book = bookService.getBook(id);
    return "bookUpdate"; // bookUpdate.xhtml
  }
    
    
  public String updateBook() {
    bookService.updateBook(book);
    return "bookList"; //.xhtml
  }
  
}
