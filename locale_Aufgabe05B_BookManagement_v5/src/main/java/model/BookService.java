package model;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Named
@ApplicationScoped
public class BookService {

//  private final List<Book> bookList;
  @PersistenceContext(unitName = "bookStore")
  private EntityManagerFactory emf;
    
  @Inject
  private IdGenerator idGenerator;
  

  public BookService() {
//    bookList = new ArrayList<>();
  }

  @Transactional
  public List<Book> getBookList() {
    EntityManager em = emf.createEntityManager();
    List<Book> bookList = em.createNamedQuery("SELECT b FROM BOOK b", Book.class).getResultList();
    return bookList;
  }
  
  @Transactional
  public void addBook(Book book) {
    book.setId(idGenerator.getId());
//    bookList.add(book);
    EntityManager em = emf.createEntityManager();
    em.persist(book);
    
  }

  @Transactional
  public void updateBook(Book book) {
//    int index = bookList.indexOf(book); // basierend auf die equals-Methode (id-basiert) der Book-Klasse
//    bookList.set(index, book);
    EntityManager em = emf.createEntityManager();
    em.merge(book);
  }
  
  
  @Transactional
  public void deleteBook(Long id) {
    Book book = getBook(id);
//    bookList.remove(book);
    EntityManager em = emf.createEntityManager();
    em.remove( em.merge(book) );
  }

  @Transactional
  public Book getBook(Long id) {
//    return findBook(id).orElse(null);
    EntityManager em = emf.createEntityManager();
    Book book = (Book) em.createQuery("SELECT b FROM BOOK b WHERE b.id=" + id).getResultList().get(0);
    return book;

  }

//  private Optional<Book> findBook(final Long id) {
//   return bookList.stream()
//            .unordered()
//            .parallel()
//            .filter( new Predicate<Book> () {
//                      @Override
//                      public boolean test(Book book) {return book.getId().equals(id); }
//                    })
//            .findFirst();
//  }
  
}
