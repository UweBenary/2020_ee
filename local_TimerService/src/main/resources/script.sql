DROP TABLE BOOK;
CREATE TABLE BOOK (
  ID        BIGINT       GENERATED ALWAYS AS IDENTITY, 
  TITLE     VARCHAR(255), 
  PUBLISHER VARCHAR(255), 
  ISBN      VARCHAR(255), 
  AUTHOR    VARCHAR(255), 
  "DATE"    VARCHAR(255), 
  PRIMARY KEY (ID)
);


INSERT INTO BOOK (AUTHOR, "DATE", ISBN, PUBLISHER, TITLE) 
	VALUES ('Christian Ullenboom', '2011', '978-3-8362-1802-3', 'Rheinwerk Computing', 'Java ist auch eine Insel');
INSERT INTO BOOK (AUTHOR, "DATE", ISBN, PUBLISHER, TITLE) 
	VALUES ('David R. Heffelfinger', '31. Januar 2015', '978-1783983520', 'Packt Publishing', 'Java EE 7 Development with NetBeans 8');
INSERT INTO BOOK (AUTHOR, "DATE", ISBN, PUBLISHER, TITLE) 
	VALUES ('Charles Lyons', '15. August 2012', '978-0955160349', 'Garner Press', 'OCEJWCD Study Companion');
INSERT INTO BOOK (AUTHOR, "DATE", ISBN, PUBLISHER, TITLE) 
	VALUES ('Antonio Goncalves', '2013', '978-1-4302-4627-5', 'Apress', 'Beginning Java EE 7');


SELECT * FROM BOOK;