package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import util.ConsoleUtil;
import util.JsfUtil;

@Interceptor // optional
public class LoggingInterceptor {

  
  
  @AroundInvoke
  @AroundTimeout
  public Object log(InvocationContext ic) throws Exception {
    String beanName = ic.getTarget().getClass().getSimpleName();
    String methodName = ic.getMethod().getName();
    ConsoleUtil.formatTitle("Entering %s.%s()", beanName, methodName);
    
    Object obj = null;
    try {
      obj = ic.proceed();
    } catch (Exception ex) {
      ex.printStackTrace();
      JsfUtil.addErrorMessage(ex, "general.error.message");
    }
    
    ConsoleUtil.formatTitle("Exiting %s.%s()", beanName, methodName);
    return obj;
  }
}
