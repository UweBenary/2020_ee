package controller;

import java.io.Serializable;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import util.ConsoleUtil;

@Named(value = "switcher")
@SessionScoped
public class LanguageSwitcher implements Serializable {

  private Locale locale = null;

  public LanguageSwitcher() {
    getLocaleFromRequest();
  }

  public String switchLocale(String lang) {
    ConsoleUtil.print("lang: " + lang);
    locale = lang.equals("x") ? null /* set browser setting */ : new Locale(lang);
    Locale.setDefault(locale);       // Einfluss auf die BeanValidation API
    return FacesContext.getCurrentInstance().getViewRoot().getViewId()
      + "?faces-redirect=true";
  }

  public Locale getLocale() {
    Locale currentLocale = (locale == null)? getLocaleFromRequest() : locale ;
    ConsoleUtil.format("LanguageSwitcher.getLocale(): Locale=%s, Current Locale=%s",locale, currentLocale);
    return currentLocale;
  }

  private Locale getLocaleFromRequest() {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
  }

}
