package model;

import controller.LoggingInterceptor;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Timer;
import javax.ejb.Stateless;
import javax.ejb.Timeout;

import javax.ejb.TimerService;
import javax.enterprise.context.RequestScoped;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.eclipse.persistence.exceptions.DatabaseException;
import util.ConsoleUtil;

//@ApplicationScoped  // CDI Bean
//@RequestScoped
@Transactional
@Stateless
@Interceptors(LoggingInterceptor.class)
public class BookService {
  
  @PersistenceContext(unitName = "myPU")
  private EntityManager em;

//  @Resource
//  private TimerService timerService;
  
  public BookService() { }
    
  // ### CREATE ###
  public void save(Book book) {
//    if (true)
//      throw new RuntimeException("something wrong with the database");
    em.persist(book); // book wird zum "attached"-Zustand übergehen
//    timerService.createTimer(5000, 3000, book); // book muss Serialisable sein
  }

  // ### READ ###
  public List<Book> getBookList() {
    String jpqlString = "SELECT b FROM Book b";
    List<Book> bookList = em.createQuery(jpqlString, Book.class).getResultList(); // bookList wird zum "attached"-Zustand übergehen
//    deleteAllTimers();
    return bookList;
  }

  public Book getBookById(long id) {
    Book book = em.find(Book.class, id); // book wird zum "attached"-Zustand übergehen
    return book;
  }

  // ### UPDATE ###
  public void update(Book book) {
    em.merge(book);                     // book wird zum "attached"-Zustand übergehen
  }

  // ### DELETE ###
  public Book delete(long id) {
    Book book = getBookById(id);       // book wird zum "attached"-Zustand übergehen
    em.remove(book);                   // book muss bereits im "attached"-Zustand sein: Das is hier der Fall wegen em.find()
    ConsoleUtil.print("Deleted Book: " + book);
    return book;
  }

//  @Timeout
//  private void programaticTimerMethod(Timer timer) {  //TimerService ruft die methode durch Container auf
//    Book book = (Book) timer.getInfo();
//    ConsoleUtil.print("Bookservice.repeat(): " + book.getTitle() + " - " + LocalTime.now());
//  }
//
//  private void deleteAllTimers() {
//    Collection<Timer> timers = timerService.getAllTimers();
//    timers.forEach(t -> t.cancel());
//  }

  @Schedule(hour = "12", minute = "27", second = "*/2", info = "some info")
  private void automaticTimerMethod(Timer timer) {  
    ConsoleUtil.print("Bookservice.repeat(): " + timer.getInfo()+ " - " + LocalTime.now());
  }
  
  
}
