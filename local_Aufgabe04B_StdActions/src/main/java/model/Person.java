package model;

public class Person {
  
  private String firstName;
  private String lastName;
  private String street;
  private String number;
  private String zipCode;
  private String city;

  public Person() {
  }

  public Person(String firstName, String lastName, String street, String number, String zipCode, String city) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.street = street;
    this.number = number;
    this.zipCode = zipCode;
    this.city = city;
  }

  
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }
  
  

}
