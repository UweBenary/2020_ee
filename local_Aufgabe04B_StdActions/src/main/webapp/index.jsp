
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--<%@include file="formCSS.css" %>--%>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<link rel="stylesheet" href="formCSS.css">-->
    
    <title>Person Form</title>
    
     <script type="text/javascript">
      function fillFormDefault() {
        document.getElementsByName("firstName")[0].value = 'Java';
        document.getElementsByName("lastName")[0].value = 'EE';
        document.getElementsByName("street")[0].value = 'Inselstr.';
        document.getElementsByName("number")[0].value = '7';
        document.getElementsByName("zipCode")[0].value = '978-3-8362-1802-3';
        document.getElementsByName("city")[0].value = 'Jakarta';
      }
      </script>
      
  </head>

  <body>
    <h1>Please, fill in this form!</h1>
    
    <form action="person.jsp" method="POST">
      <table>
        <tr>
          <td>
            <label for="firstName">First Name</label>
          </td>
          <td>
            <input id="firstName" type="text" name="firstName" required="required" />         
          </td>
        </tr>
        
        <tr>
          <td>
            <label for="lastName">Last Name</label>
          </td>
          <td>
            <input id="lastName" type="text" name="lastName" required="required" />         
          </td>
        </tr>
        
        <tr>
          <td>
            <label for="street">Street</label>
          </td>
          <td>
            <input id="street" type="text" name="street" required="required" />         
          </td>
        </tr>
        
        <tr>
          <td>
            <label for="number">Number</label>
          </td>
          <td>
            <input id="number" type="text" name="number" required="required" min="1" />    

          </td>
        </tr>
        
        <tr>
          <td>
            <label for="zip-code">ZIP</label>
          </td>
          <td>
            <input id="zip-code" type="text" name="zipCode" />         
          </td>
        </tr>
        
        <tr>
          <td>
            <label for="number">City</label>
          </td>
          <td>
            <input id="city" type="text" name="city" required="required" />         
          </td>
        </tr>
        
      </table>
      
      <br/><br/>
      
      <table>
        <tr>
          <td colspan="2">
            <input type="button" value="Click'n'Fill" onclick="fillFormDefault()" />
            <input type="reset" value="Reset" />       
            <input type="submit" value="Save" />  
          </td>
        </tr>
      </table>
           
      
    </form>
  </body>
</html>
