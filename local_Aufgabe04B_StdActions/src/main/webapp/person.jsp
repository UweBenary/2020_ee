<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Person</title>
  </head>
  <body>
    <h1>Person data:</h1>
    
    <jsp:useBean id="person" class="model.Person" scope="request" >
      <jsp:setProperty name="person" property="*"/>
    </jsp:useBean>
    
    
    <table border="0" >
      <!-- 1. Zeile-->
      <tr>
        <td>First name:</td>
        <td><jsp:getProperty name="person" property="firstName" /></td>
      </tr>
      <tr>
        <td>Last name:</td>
        <td><jsp:getProperty name="person" property="lastName" /></td>
      </tr>
      <tr>
        <td>Street/number:</td>
        <td><jsp:getProperty name="person" property="street" /> / <jsp:getProperty name="person" property="number" /></td>
      </tr>
      <tr>
        <td>City/ZIP:</td>
        <td><jsp:getProperty name="person" property="city" /> / <jsp:getProperty name="person" property="zipCode" /></td>
      </tr>
     </table>
    
    
  </body>
</html>
