<%-- 
    Document   : index
    Created on : 15.09.2015, 10:36:20
    Author     : tverrbjelke
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Status</title>
        <link rel="stylesheet" href="main.css" /> 
    </head>
    <body>
      
        <jsp:include page="header.jsp" />
        <h1>Status</h1>
        <jsp:include page="status_footer.jsp" />
        
    </body>
</html>
