<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:if test="${not empty info or not empty error}">
  <hr style="color: black; height: 1px"/>
  <div style="color:green">${info}</div>
  <div style="color:red">${error}</div>
</c:if>
  