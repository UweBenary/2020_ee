package controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/MS")
public class MyServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   
    // controller tasks
    String todo = request.getParameter("todo");
    todo = (todo == null)? "" : todo;
    // model
    
    // view
    switch (todo) {
      case "test1":
        test1(request, response); break;
      case "test2":
        test2(request, response); break;
      case "test3":
        test3(request, response); break;
      default:
        error(request, response);
    }

  }

  private void test3(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    response.setContentType("text/html;charset=UTF-8;");// besser
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>MyServlet</title></head>");
    out.println("<body>");
    out.println("<h1>MyServlet</h1>");
    out.println("<p>test3</p>");
    out.println("</body>");
    out.println("</html>");
  }
  
  private void test2(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //    response.setContentType("text/plain");
    //    response.setCharacterEncoding("UTF-8");
    response.setContentType("text/plain;charset=UTF-8;");// besser
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>MyServlet</title></head>");
    out.println("<body>");
    out.println("<h1>MyServlet</h1>");
    out.println("</body>");
    out.println("</html>");
  }
  
  private void test1(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //    response.setContentType("text/plain");
    //    response.setCharacterEncoding("UTF-8");
    response.setContentType("text/html;charset=UTF-8;");// besser
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>MyServlet</title></head>");
    out.println("<body>");
    out.println("<h1>MyServlet</h1>");
    out.println("<p>test1</p>");
    out.println("<p>" + LocalTime.now() + "</p>");
    out.println("</body>");
    out.println("</html>");
  }

  private void error(HttpServletRequest request, HttpServletResponse response) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
  

}
