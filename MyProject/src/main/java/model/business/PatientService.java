package model.business;

import java.util.List;
import util.interceptor.LoggingInterceptor;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.persistence.Patient;

@Stateless
@Interceptors(LoggingInterceptor.class)
public class PatientService {

  @PersistenceContext(unitName = "ccccDB")
  private EntityManager em;

  public void save(Patient patient) {
    em.persist(patient);
  }

  public List<Patient> getAllPatients() {
    String jpqlString = "SELECT p FROM Patient p";
    List<Patient> bookList = em.createQuery(jpqlString, Patient.class).getResultList();
    return bookList;
  }

  public Patient getPatientById(long id) {
    Patient patient = em.find(Patient.class, id);
    return patient;
  }

  public void update(Patient book) {
    em.merge(book);
  }

  public Patient delete(long id) {
    Patient patient = getPatientById(id);
    em.remove(patient);
    return patient;
  }

}
