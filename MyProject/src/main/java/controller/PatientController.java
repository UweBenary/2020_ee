package controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.Interceptors;
import model.business.PatientService;
import model.persistence.Patient;
import util.JsfMessaging;
import util.interceptor.LoggingInterceptor;

@RequestScoped
@Named
@Interceptors(LoggingInterceptor.class)
public class PatientController implements Serializable {

  @Inject
  private PatientService patientService;

  private Patient patient;
  private List<Patient> patientList;

  public PatientController() {
    patient = new Patient();
  }

  public Patient getPatient() {
    return patient;
  }

  public void save() {
    patientService.save(patient);
    JsfMessaging.addSuccessMessage("patient.create.success", patient.toString());
  }

  public String delete(long id) {
    Patient deletedPatient = patientService.delete(id);
    JsfMessaging.addSuccessMessage("patient.delete.success", deletedPatient.toString());
    return "patientList"; //patientList.html
  }

  public List<Patient> getPatientList() {
    if (patientList == null) {
      patientList = patientService.getAllPatients();
    }
    return patientList;
  }

}
