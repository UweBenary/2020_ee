package controller;

import util.interceptor.LoggingInterceptor;
import java.io.Serializable;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.interceptor.Interceptors;

@Named
@SessionScoped
//@Interceptors(LoggingInterceptor.class)
public class LocaleSwitcher implements Serializable {
  static final long VERSION = 1L;
  
  private Locale locale;
  
  public LocaleSwitcher() {
    locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
  }

  public Locale getLocale() {
    System.out.println("Get: " + this);
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;    
    FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
  }
  
  public String switchLocale(String localeString) {
    locale = new Locale(localeString);
    setLocale(locale);
    return FacesContext.getCurrentInstance().getViewRoot().getViewId()
      + "?faces-redirect=true";
  }

  @Override
  public String toString() {
    return "Locale: " + locale; }
  
  
}
