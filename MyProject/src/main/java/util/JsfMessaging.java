package util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class JsfMessaging {

  private static String msg(String key, Object... parameters) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    String messageBundleName = facesContext.getApplication().getMessageBundle();
    Locale locale = facesContext.getViewRoot().getLocale();
    ResourceBundle bundle = ResourceBundle.getBundle(messageBundleName, locale);
    return MessageFormat.format(bundle.getString(key), parameters);
  }

  public static void addSuccessMessage(String msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
    FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
  }

  public static void addSuccessMessage(String key, Object... parameters) {
    String msg = msg(key, parameters);
    addSuccessMessage(msg);
  }

  public static void addErrorMessage(String msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
  }

  public static void addErrorMessage(String key, Object... parameters) {
    String msg = msg(key, parameters);
    addErrorMessage(msg);
  }

  public static void addErrorMessage(Exception ex, String key, Object... parameters) {
    addErrorMessage(key, parameters);
    do {
      addErrorMessage(ex.toString());
    } while ((ex = (Exception) ex.getCause()) != null);
  }

}
