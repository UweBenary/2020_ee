package util.logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
//@ApplicationScoped
@Named 
public class ViewLogger implements Serializable {
  
  private final List<String> successMessages;
  private final List<String> errorMessages;

  public ViewLogger() {
    successMessages = new ArrayList<>();
    errorMessages = new ArrayList<>();
//    init();
  }  

  private void init() {
    addSuccessMessage("success");  
    addSuccessMessage("success2");  
    addErrorMessages("error");  
    addErrorMessages("error2");  
  }
  
  
  public List<String> getSuccessMessages() {
    return successMessages;
  }
  
  public List<String> getErrorMessages() {
    return errorMessages;
  }

  public void addSuccessMessage(String successMessage) {
    successMessages.add(successMessage);
  }

  public void addErrorMessages(String errorMessage) {
    errorMessages.add(errorMessage);
  }

  
  
  
  

}
