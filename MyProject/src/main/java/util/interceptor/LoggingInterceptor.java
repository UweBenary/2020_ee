package util.interceptor;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.interceptor.AroundConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import util.logger.ViewLogger;

@Dependent
@Interceptor
public class LoggingInterceptor implements Serializable {

  @Inject
  private ViewLogger viewLogger;

  public LoggingInterceptor() {
    System.out.println("====> interceptor.LoggingInterceptor.<init>()");
  }

  @AroundConstruct
  private void interceptConstructor(InvocationContext ic) throws Exception {
    String beanName = getBeanName(ic);
    try {
      ic.proceed();
      viewLogger.addSuccessMessage("LoggingInterceptor: Calling constructor of " + beanName);
    } catch (Exception ex) {
      final String errorString = String.format("LoggingInterceptor -> Error when calling constructor of %s : ", beanName);
      viewLogger.addErrorMessages(errorString + ex.getMessage());
//      Logger.getLogger(LoggingInterceptor.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @PostConstruct
  private void interceptPostConstruct(InvocationContext ic) {
    String beanName = getBeanName(ic);
    try {
      ic.proceed();
      viewLogger.addSuccessMessage("LoggingInterceptor: @PostConstruct of " + beanName);
    } catch (Exception ex) {
      final String errorString = String.format("LoggingInterceptor -> Error at @PostConstruct of %s : ", beanName);
      viewLogger.addErrorMessages(errorString + getExceptionInfo(ex) );
//      Logger.getLogger(LoggingInterceptor.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @AroundInvoke
  public Object interceptAroundInvoke(InvocationContext ic) {
    String beanAndMethodName = getBeanAndMethodName(ic);

    Object obj = null;
    try {
      obj = ic.proceed();
      viewLogger.addSuccessMessage("LoggingInterceptor: Invokation of " + beanAndMethodName);
    } catch (Exception ex) {
      final String errorString = String.format("LoggingInterceptor -> Invokation error around %s : ", beanAndMethodName);
      viewLogger.addErrorMessages(errorString + getExceptionInfo(ex));
    }

    return obj;
  }

  @PreDestroy
  private void interceptPreDestroy(InvocationContext ic) {
    String beanName = getBeanName(ic);
    try {
      ic.proceed();
      viewLogger.addSuccessMessage("LoggingInterceptor: @PreDestroy of " + beanName);
    } catch (Exception ex) {
      final String errorString = String.format("LoggingInterceptor -> Error at @PreDestroy of %s : ", beanName);
      viewLogger.addErrorMessages(errorString + getExceptionInfo(ex));
//      Logger.getLogger(LoggingInterceptor.class.getName()).log(Level.SEVERE, null, ex);
    }
  }


  private static String getExceptionInfo(Exception ex) {
    return ex.getMessage()==null? ex.toString() : ex.getMessage();
  }

  private String getBeanAndMethodName(InvocationContext ic) {
    String result;
    String beanName = getBeanName(ic);
    try {
      String methodName = ic.getMethod().getName();
      result = String.format("%s.%s()", beanName, methodName);
    } catch (Exception ex) {
      viewLogger.addErrorMessages("LoggingInterceptor.getBeanAndMethodName() messaging error: " + ex.toString());
      result = beanName;
    }
    return result;
  }

  private String getBeanName(InvocationContext ic) {
    String beanName = "someUnknownBean";
    try {
      beanName = ic.getTarget().getClass().getSimpleName();
      if (beanName.contains("$")) {
        beanName = beanName.substring(0, beanName.indexOf("$")) + " (Proxy)";
      }
    } catch (Exception ex) {
      viewLogger.addErrorMessages("LoggingInterceptor.getBeanName() messaging error: " + ex.toString());
    }
    return beanName;
  }
}
