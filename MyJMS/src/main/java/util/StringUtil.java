package util;
public class StringUtil {
  
  public static String cleaned(String beanName) {
    return cleaned(beanName, true);
  }

  public static String cleaned(String beanName, boolean retainProxy) {
    if (beanName.contains("$"))
      beanName = beanName.substring(0, beanName.indexOf("$")) 
                 + ( retainProxy ? " (Proxy)" : "" );
    return beanName;
  }

  public static String simpleNameCleaned(Object bean) {
    return cleaned(bean.getClass().getSimpleName());
  }
}
