package util;

public class ConsoleUtil {

  public static void printTitle(String message, char decorationChar, int lineWidth, boolean withSeperator) {
//    if (message.toString().length() >= lineWidth)
//      lineWidth = message.toString().length() + 10;
//    System.out.println("====> message.length() " + message.length());
    if (message.length() > lineWidth - 4)
      message = message.substring(0, lineWidth - 7) + "...";
//    System.out.println("====> message.length() " + message.length());
    printSeperator(decorationChar, lineWidth);
    int leftMargin = (lineWidth - 2 + message.length()) / 2;
    int rightMargin = lineWidth - 1 - leftMargin;
    String titleFormat = decorationChar + "%" + leftMargin + "s" + "%" + rightMargin + "s";
//    System.out.println("====> titleFormat: " + titleFormat);
    System.out.println(String.format(titleFormat, message, decorationChar + ""));
    if (withSeperator) {
      printSeperator(decorationChar, lineWidth);
    }
  }

  public static void printTitle(String message, char decorationChar, final int LINE_WIDTH) {
    printTitle(message, decorationChar, LINE_WIDTH, true);
  }

  public static void printTitle(String message) {
    printTitle(message, '#', TIMES);
  }

  public static void printTitle(String message, boolean formattedLines, String... lines) {
    printTitle(message, '#', TIMES, false);
    if (formattedLines) {
      for (int i = 0; i < lines.length - 1; i = i+2) {
        format(lines[i], '#', lines[i + 1]);
      }
    } else {
      for (int i = 0; i < lines.length - 1; i++) {
        print(lines[i], '#');
      }
    }
    printSeperator('#', TIMES);
    System.out.println("\n");
  }
  private static final int TIMES = 100;

  public static void format(String StringFormat, char decorationChar, String arg) {
    print(String.format(StringFormat, arg), decorationChar);
  }

  public static void format(String StringFormat, Object... objects) {
    print(String.format(StringFormat, objects));
  }

  public static void formatTitle(String StringFormat, Object... objects) {
    printTitle(String.format(StringFormat, objects));
  }

  public static void print(Object message) {
//    int margin = (80 - message.length() - 2) / 2;
//    String decoration = "";
//    for (int i = 0; i < margin; i++) {
//      decoration += '=';
//    }
//    System.out.format("=======> %s%n", message);
    System.out.format("=======> %s", message);
  }
  
  public static void print(Object message, char decorationChar) {
//    System.out.format(decorationChar + " =======> %s%n", message);
    System.out.format(decorationChar + " =======> %s", message);
  }

  private static void printSeperator(char decorationChar, int lineWidth) {
    String seperator = "";
    for (int i = 0; i < lineWidth; i++) {
      seperator += decorationChar;
    }
//    seperator += "\n";
    System.out.println(seperator);
  }
  

}
