package model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/inventar")
  }
)
public class InventarMDB implements MessageListener {

  @Override
  public void onMessage(Message msg) {
    try {
      Bestellung bestellung = msg.getBody(Bestellung.class);
      System.out.println(bestellung + " von MDB empfangen");
    } catch (JMSException ex) {
      Logger.getLogger(InventarMDB.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}


