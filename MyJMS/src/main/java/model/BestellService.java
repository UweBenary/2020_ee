package model;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.Destination;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;

@Stateless
public class BestellService {

  @Inject
  @JMSConnectionFactory("jms/myCF")
  private JMSContext jMSContext;
  
  @Resource(lookup = "jms/inventar")
  private Queue inventarQueue; //geht auch Destination, Queue oder Topic
  
  public void sendMessage(Bestellung bestellung) {
    JMSProducer producer = jMSContext.createProducer();
    producer.send(inventarQueue, bestellung);
  }
  
  public Bestellung consumeManuallyMessage() {
    JMSConsumer consumer = jMSContext.createConsumer(inventarQueue);
    Bestellung bestellung = consumer.receiveBody(Bestellung.class,3000);
    System.out.println("====> BEstellung empfangen - id: " + bestellung.getId());
    return bestellung;
  }
  
}
