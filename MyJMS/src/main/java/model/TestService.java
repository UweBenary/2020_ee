package model;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TestService {

  @PersistenceContext(unitName = "myPU")
  private EntityManager em;
  
  public String getMessage1() {
    return "Message1 From TestService";
  }
  
  
}
