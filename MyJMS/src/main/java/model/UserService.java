package model;

import interceptor.LoggingInterceptor;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import util.ConsoleUtil;

//@ApplicationScoped  // CDI Bean
@RequestScoped
@Transactional
@Interceptors(LoggingInterceptor.class)
//@Stateless
public class UserService {

  @PersistenceContext(unitName = "myPU")
  private EntityManager em;

  public UserService() {
  }

  // ### CREATE ###
  public void save(User user) {
//    if (true)
//      throw new RuntimeException("something wrong with the database");
    em.persist(user); // user wird zum "attached"-Zustand übergehen
  }

  // ### READ ###
  public List<User> getUserList() {
    String jpqlString = "SELECT u FROM User u ORDER BY u.lastname";
    List<User> userList = em.createQuery(jpqlString, User.class).getResultList(); // userList wird zum "attached"-Zustand übergehen
    return userList;
  }

  public User getUserById(long id) {
    User user = em.find(User.class, id); // user wird zum "attached"-Zustand übergehen
    return user;
  }

  // ### UPDATE ###
  public void update(User user) {
    em.merge(user);                     // user wird zum "attached"-Zustand übergehen
  }

  // ### DELETE ###
  public User delete(long id) {
    User user = getUserById(id);       // user wird zum "attached"-Zustand übergehen
    em.remove(user);                   // user muss bereits im "attached"-Zustand sein: Das is hier der Fall wegen em.find()
    ConsoleUtil.print("Deleted User: " + user);
    return user;
  }

  public void reset() {
    em.createQuery("DELETE FROM User").executeUpdate();
//    em.createNativeQuery("DELETE FROM USER_T").executeUpdate(); // Native SQL
    em.flush();
    em.persist(new User("Peter", "Schmidt"));
    em.persist(new User("Hans", "Meyer"));
    em.persist(new User("Thomas", "Arnold"));
  }

}
