package controller;

import interceptor.LoggingInterceptor;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.Interceptors;
import model.User;
import model.UserService;
import util.ConsoleUtil;
import util.JsfUtil;

@RequestScoped
//@SessionScoped
@Named  // (für die Erreichbarkeit in JSF Facelets)
@Interceptors(LoggingInterceptor.class)
public class UserController implements Serializable {

  private Action action;
  
  @Inject
  private UserService userService;

  private User user;

  private List<User> userList;

  public UserController() {
    user = new User();
  }

  public User getUser() {
    return user;
  }

  public void saveUpdate() {
    if (isEditing())
      update();
    else
      save();
  }
  public void save() {
      userService.save(user);
      updateUserListFromDatabase();
      JsfUtil.addSuccessMessage("create.success.message", user.toString());
  }

  public List<User> getUserList() {
    // Um Mehrfach-Aufrufe vom UserService zu vermeiden !!!
    if (userList == null) {
      updateUserListFromDatabase();
    }
    return userList;
  }

  public void delete(long id) {
      this.user = userService.delete(id);
      updateUserListFromDatabase();
      JsfUtil.addSuccessMessage("delete.success.message", user.toString());
      resetUser();
  }

  public void edit(long id) {
    user = userService.getUserById(id);
    action = Action.EDIT;
  }

  public void update() {
    userService.update(user);
    updateUserListFromDatabase();
    JsfUtil.addSuccessMessage("update.success.message", user.toString());
  }

  public String log(String msg) {
    ConsoleUtil.print(msg);
    return msg;
  }

  private void updateUserListFromDatabase() {
    ConsoleUtil.print("UserController.updateUserListFromDatabase()");
    userList = userService.getUserList();
  }

  public void dbReset() {
    userService.reset();
    updateUserListFromDatabase();
    resetUser();
    JsfUtil.addSuccessMessage("reset.success.message", "");
  }

  public void formReset() {
    setEditing(false);
    resetUser();
  }
  
  public boolean isEditing() {
    return action == Action.EDIT;
  }
  public void setEditing(boolean editing) {
    if (editing)
      action = Action.EDIT;
    else
      action = null;
  }

  private void resetUser() {
    user = new User();
  }

}
