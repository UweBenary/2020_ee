package controller;

import java.util.Random;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.BestellService;
import model.Bestellung;
import util.JsfUtil;

@Named
@RequestScoped
public class BestellController {

  @Inject
  private BestellService bestellService;
  
  public void bestellen(){
//    Bestellung bestellung = new Bestellung( new Random(1000).nextLong());
    Bestellung bestellung = new Bestellung( (long) new Random().nextInt(1000));
    bestellService.sendMessage(bestellung);
    JsfUtil.addSuccessMessage("Bestellung mit id: "+ bestellung.getId() + " wurde abgeschickt.");
  }
  
  public void empfangen() {
    try {
    Bestellung bestellung = bestellService.consumeManuallyMessage();
    JsfUtil.addSuccessMessage("Bestellung mit id: "+ bestellung.getId() + " wurde empfangen.");
      
    } catch (Exception e) {
      JsfUtil.addErrorMessage("Fehler: Bestellung noch nicht empfangen");
    }
  }
  
}
