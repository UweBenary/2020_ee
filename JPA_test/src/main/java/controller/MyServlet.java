package controller;

import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Person;


@WebServlet("/MS")
public class MyServlet extends HttpServlet {
  
  @PersistenceContext(unitName = "myPU")
  private EntityManager em;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/plain");
    List<Person> personList = em.createQuery("SELECT p FROM Person p", Person.class).getResultList();
    resp.getWriter().println(personList);
  }
  
  

}
