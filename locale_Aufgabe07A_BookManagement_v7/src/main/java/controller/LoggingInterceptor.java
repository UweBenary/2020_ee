package controller;


import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import util.JsfUtil;

@Interceptor
public class LoggingInterceptor {
  
  @AroundInvoke
  public Object log(InvocationContext ic) {
    Object obj = null;
    try {
      obj =  ic.proceed();
    } catch (Exception e) {
      e.printStackTrace();
      JsfUtil.addErrorMessage(e, "general.errror.message");
    }
    return obj;
  }

}
