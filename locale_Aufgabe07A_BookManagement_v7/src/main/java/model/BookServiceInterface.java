/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Your Name <yourname@comcave.com>
 */
@Local
public interface BookServiceInterface {
  
  void save(Book book) throws Exception ;
  
  List<Book> getBookList() ;

  Book getBookById(long id) ;
  
  void update(Book book) ;

  Book delete(long id) ;
}
