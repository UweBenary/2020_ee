package model;


import java.sql.SQLException;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;



//@ApplicationScoped   // CDI Bean
//@RequestScoped
@Transactional
@Stateless
@LocalBean // no interface view
@Local(BookServiceInterface.class) // local interface view
@Remote(BookServiceInterfaceRemote.class) // remote interface view
public class BookService implements BookServiceInterface, BookServiceInterfaceRemote {
  
  @PersistenceContext(unitName = "myPU")
  private EntityManager em;

  public BookService() {
  }

  @Override
  public void save(Book book) throws Exception {
//    if (true) {
//      throw new RuntimeException(new SQLException("Some issue"));
//    }
    em.persist(book);
  }

  @Override
  public List<Book> getBookList() {
    String jpqlString = "SELECT b FROM Book b";
    List<Book> bookList = em.createQuery(jpqlString,Book.class).getResultList();
    return bookList;
  }

  @Override
  public Book getBookById(long id) {
    Book book = em.find(Book.class, id);
    return book;
  }

  @Override
  public void update(Book book) {
    em.merge(book);
  }

  @Override
  public Book delete(long id) {
    Book book = getBookById(id);
    em.remove(book);
    return book;
  }
  
}
