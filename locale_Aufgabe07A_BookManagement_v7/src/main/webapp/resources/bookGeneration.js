function fillForm1() {
  document.getElementsByName("formId:title")[0].value = 'Java ist auch eine Insel';
  document.getElementsByName("formId:publisher")[0].value = 'Rheinwerk Computing';
  document.getElementsByName("formId:isbn")[0].value = '978-3-8362-1802-3';
  document.getElementsByName("formId:author")[0].value = 'Christian Ullenboom';
  document.getElementsByName("formId:date")[0].value = '2011';
  return false;
}
function fillForm2() {
  document.getElementsByName("formId:title")[0].value = 'Java EE 7 Development with NetBeans 8';
  document.getElementsByName("formId:publisher")[0].value = 'Packt Publishing';
  document.getElementsByName("formId:isbn")[0].value = '978-1783983520';
  document.getElementsByName("formId:author")[0].value = 'David R. Heffelfinger';
  document.getElementsByName("formId:date")[0].value = '31. Januar 2015';
  return false;
}
function fillForm3() {
  document.getElementsByName("formId:title")[0].value = 'OCEJWCD Study Companion';
  document.getElementsByName("formId:publisher")[0].value = 'Garner Press';
  document.getElementsByName("formId:isbn")[0].value = '978-0955160349';
  document.getElementsByName("formId:author")[0].value = 'Charles Lyons';
  document.getElementsByName("formId:date")[0].value = '15. August 2012';
  return false;
}
function fillForm4() {
  document.getElementsByName("formId:title")[0].value = 'Beginning Java EE 7';
  document.getElementsByName("formId:publisher")[0].value = 'Apress';
  document.getElementsByName("formId:isbn")[0].value = '978-1-4302-4627-5';
  document.getElementsByName("formId:author")[0].value = 'Antonio Goncalves';
  document.getElementsByName("formId:date")[0].value = '2013';
  return false;
}
