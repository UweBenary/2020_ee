package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HtmlCode;


//@WebServlet(value={"/Start","/*"})
//@WebServlet("/Start")
public class MyServlet extends HttpServlet {

  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    String outputString;
        
    String todo = request.getParameter("todo");
    if (todo == null) {     
      outputString = getStartPage(request, response);
    } else {      
      outputString = getDoGetResponsePage(request, response);
    }    
    
    writeResponse(request, response, outputString );
  }
  
  public String getStartPage(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    
    List<String> links2add = links(request, response);
    String output = HtmlCode.writeLinks(links2add);
    return HtmlCode.writeHtml(output);      
  }

  private List<String> links(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    List<String> result = new ArrayList<>();
    
    final Enumeration<String> initParameterNames = getServletConfig().getInitParameterNames();
    while (initParameterNames.hasMoreElements()) {
      String nextElement = initParameterNames.nextElement();
      String link = getServletConfig().getInitParameter(nextElement);
      result.add( "<a href=\"" + link + "\">" + nextElement.substring(4) + "</a>");
    }
  
    return result;
  }
  
  public String getDoGetResponsePage(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    String output = "<p>doGet() called at " + LocalTime.now() + "</p>";
    return HtmlCode.writeHtml(output);  
  }
  
  
     
  private void writeResponse(HttpServletRequest request, HttpServletResponse response, String outString) 
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8;");
    PrintWriter out = response.getWriter();
    out.println( outString );
  }


  
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String output = getDoPostResponsePage(request, response);
    writeResponse(request, response, output);
  }

  public String getDoPostResponsePage(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
    String input = request.getParameter("input");
    String output = "Your input: " + input;
    return HtmlCode.writeHtml(output);

  }
 
}
