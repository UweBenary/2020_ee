package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HtmlCode {


  public static String writeHtml(String outString, String... more) {
    StringBuilder htmlText = new StringBuilder();
    htmlText.append("<html>")
    .append("<head><title>MyServlet</title></head>")
    .append("<body>")
    .append("<a href=\"Start\">Home</a>")
    .append("<h1>My Servlet</h1>")
    .append(outString);
    for (String s : more) {
      htmlText.append(s);
    }
    htmlText.append("</body>")
      .append("</html>");
    return htmlText.toString();
  }

  public static String writeLinks(List<String> links2add) {
    List<String> links = getDefaultLinks();
                            
    if (links2add != null) 
      links.addAll(links2add);  
    
    return buildUnorderedList(links);
  }

  private static String buildUnorderedList(List<String> links) {
    StringBuilder result = new StringBuilder("<ul>");
    for (String link : links) {
      result.append("<li>")
            .append( link )
            .append("</li>");
    }
    result.append("</ul>");
    return result.toString();
  }

  private static List<String> getDefaultLinks() {
    String[] defaultLinks = { "<a href=\"Start?todo=doget\">doGet()</a>",
                              "<a href=\"form.jsp\">doPost()</a>" };
    return new ArrayList<>( Arrays.asList(defaultLinks) );
  }

}
